//Создайте по 2 enum с разным типом RawValue
enum MonthDays: Int {
    case january = 31, february = 28, march = 31, april = 30, may = 31, june = 30, july = 31, august = 31, september = 30,
         october = 31, november = 30, december = 31
}

enum Cosmetics: String {
    case lipstick
    case mascara
    case foundation
    case eyeshadow
    case blush
}



//Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж
enum sex {
    case male
    case female
    case other
}

enum Age {
    case teenager
    case young
    case adult
    case old
}

enum Experience {
    case intern
    case junior
    case middle
    case senior
}



//Создать enum со всеми цветами радуги
enum rainbowColor: String {
    case красный = "red"
    case оранжевый = "orange"
    case желтый = "yellow"
    case зеленый = "green"
    case голубой = "blue"
    case синий = "sapphire"
    case фиолетовый = "purple"
}



//Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple
//green', 'sun red' и т.д.
enum Cosmetics{
    case lipstick(String)
    case mascara(String)
    case foundation(String)
    case eyeshadow(String)
}

func printCosmetics() {
    let cosmeticsArray: [Cosmetics] = [.lipstick("Red"), .mascara("Black"), .foundation("Light"), .eyeshadow("Blue")]

    for cosmetics in cosmeticsArray {
        switch cosmetics {
        case .lipstick(let color):
            print("Lipstick - \(color)")
        case .mascara(let color):
            print("Mascara - \(color)")
        case .foundation(let shade):
            print("Foundation - \(shade)")
        case .eyeshadow(let color):
            print("Eyeshadow - \(color)")
        }
    }
}

printCosmetics()


//Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы}
//и выводит числовое значение оценки
enum Score: String {
    case A = "Отлично"
    case B = "Хорошо"
    case C = "Удовлетворительно"
    case D = "Неудовлетворительно"
}

func setGrade(score: Score) {
    switch score {
    case .A:
        print("Оценка: 5")
    case .B:
        print("Оценка: 4")
    case .C:
        print("Оценка: 3")
    case .D:
        print("Оценка: 2")
    }
}



//Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum
enum LuxuryCar: String {
    case mercedes = "Mercedes-Benz"
    case bmw = "BMW"
    case audi = "Audi"
    case porsche = "Porsche"

    static var garage: [LuxuryCar] = [.mercedes, .bmw, .audi, .porsche]

    static func printGarage() {
        print("Автомобили в гараже:")
        for car in garage {
            print(car.rawValue)
        }
    }
}

LuxuryCar.printGarage()
