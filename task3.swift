//Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную
var numbers = [6, 3, 8, 1, 11, 5]

// Сортировка в одну сторону
numbers.sort { $0 < $1 }
print(numbers)

// Сортировка в обратную сторону
numbers.sort { $0 > $1 }
print(numbers)




// Создать метод, который принимает имена друзей, после этого имена положить в массив
// Массив отсортировать по количеству букв в имени
func sortFriends(_ friends: [String]) -> [String] {
    let sortedFriends = friends.sorted { $0.count < $1.count }
    return sortedFriends
}

// Пример использования
let myFriends = ["Ксения", "Ингиборга", "Миша", "Ян"]
let sortedFriends = sortFriends(myFriends)
print(sortedFriends)




//Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга
//Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение
let friends = ["Аня", "Ксения", "Ингиборга", "Ян", "Дима"]

var dictionary: [Int: String] = [:]

for friend in friends {
    let length = friend.count
    dictionary[length] = friend
}

func printValueForKey(_ key: Int) {
    if let friendName = dictionary[key] {
        print("\(key): \(friendName)")
    }
}
printValueForKey(9) // выведется ингиборга
