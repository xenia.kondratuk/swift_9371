//Описать несколько структур – любой легковой автомобиль и любой грузовик. Структуры должны содержать марку авто, год выпуска,
//объем багажника/кузова, запущен ли двигатель, открыты ли окна, заполненный объем багажника

//Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия
//для легкового авто
struct Car {
    let brand: String
    var year: Int
    var trunkVolume: Int
    var engineRunning: Bool
    var windowsOpen: Bool
    var trunkFilledVolume: Int
    
    mutating func performAction(_ action: CarAction) {
        switch action {
        case .startEngine:
            engineRunning = true
        case .stopEngine:
            engineRunning = false
        case .openWindows:
            windowsOpen = true
        case .closeWindows:
            windowsOpen = false
        case .loadCargo(let volume):
            if Int(volume) < (trunkVolume - trunkFilledVolume) {
                trunkFilledVolume += Int(volume)
            } else {
                print("Not enough space in the trunk")
            }
        case .unloadCargo(let volume):
            if Int(volume) < trunkFilledVolume {
                trunkFilledVolume -= Int(volume)
            } else {
                print("Not enough cargo in the trunk")
            }
        }
    }
}


//грузовик
struct Truck {
    let brand: String
    let year: Int
    var cargoVolume: Double
    var engineIsRunning: Bool
    var cargoVolumeFilled: Double = 0.0
}



//Описать перечисление с возможными действиями с автомобилем: запустить/заглушить двигатель, открыть/закрыть окна,
//погрузить/выгрузить из кузова/багажника груз определенного объема
enum CarAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadCargo(volume: Double)
    case unloadCargo(volume: Double)
}



