//Создать класс родитель и 2 класса наследника
//класс родитель
class Cosmetic {
    let name: String
    let brand: String
    let price: Double
    
    //инициализация
    init(name: String, brand: String, price: Double) {
        self.name = name
        self.brand = brand
        self.price = price
    }
}

//класс наследник
class Lipstick: Cosmetic {
    let color: String
    
    init(name: String, brand: String, price: Double, color: String) {
        self.color = color
        super.init(name: name, brand: brand, price: price)
    }
}

//класс наследник
class Eyeshadow: Cosmetic {
    let palette: String
    
    init(name: String, brand: String, price: Double, palette: String) {
        self.palette = palette
        super.init(name: name, brand: brand, price: price)
    }
}



//Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*
//(выводит площадь),*destroy*(отображает что дом уничтожен)
class House {
    var width: Double
    var height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("Площадь дома равна \(area) кв. метров")
    }
    
    func destroy() {
        print("Дом уничтожен")
    }
}



//Создайте класс с методами, которые сортируют массив учеников по разным параметрам
class StudentsSorter {
    
    // Сортировка по имени
    func sortByName(_ students: inout [String]) {
        students.sort()
    }
    
    // Сортировка по успеваемости
    func sortByGrades(_ students: inout [(name: String, grade: Double)]) {
        students.sort { $0.grade > $1.grade }
    }
}



//Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
//класс
class Cosmetic {
    let name: String
    var brand: String
    var price: Double
    
    init(name: String, brand: String, price: Double) {
        self.name = name
        self.brand = brand
        self.price = price
    }
}

//структура
struct CosmeticBrand {
    let name: String
    let country: String
    var popularProducts: [Cosmetic]
}

let chanel = CosmeticBrand(name: "Chanel", country: "France", popularProducts: [mascara])

//Классы — ссылочный тип, структуры - значимый типами. При работе со структурами происходит копирование данных, а при работе
//с классами - передача ссылок на данные
