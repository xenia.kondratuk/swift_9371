let name = "Ksenia"
let shortName = "Ksenia"
let surname = "Kondratuk"
let age = 22
let hometown = "Crimea"
let secondHome = "Saint-Petersburg"
let job = "QA Engineer"
let company = "VibeLab"

let hobby = ["youtube", "makeup", "songs"]

let university = "ETU LETI"
let course = 4

print("About me: ")
print("Hello dear, who is checking my home work right now! I'm going to tell to you a bit information about me")
print("My name is \(name). My close people call me \(shortName). I'm \(age) years old. I was born in a fantasy place — \(hometown). When I was 17, I moved to the \(secondHome) to study at university. As of today, I'm student of \(university) on \(course) year")
print("I work as a \(job) in the \(company). Most of the my freetime I watch \(hobby[0]) or do \(hobby[1]). It's my pleasure.")

print("Also, sometimes I like to translate english \(hobby[2]) to russian. In this way, I listen amazing music and learn English at the same time")

print("Thank you for reading!")





